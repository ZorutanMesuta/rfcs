
- Feature Name: steel_worms_buffet
- Start Date: 2021-04-18
- RFC Number: 
- Tracking Issue: 

# Summary

A new dungeon type, using spaghetti cave generation. Several variatiants, several new enemies, items, features, debuffs, behaviours and lore.

# Motivation

Lack of dungeon types, 

# Guide-level explanation

# Reference-level explanation

# Drawbacks

# Rationale and alternatives

# Prior art

# Unresolved questions












# Steel Worms Buffet
New dungeon type with several varations to keep it interesting and fresh!

## [Hard] Variation 1 (Active Buffet)

### Enemies

Steel Worm Babies: They attack by going into the wall, and suddenly bursting out from a random direction at you. If you can dodge the attack, they will be stunned for a little bit so you can damage them, and then they'll dig back into the walls to attack again. They are pretty tough and do fairly high amounts of damage (since their attack is dodged fairly easily). You can also hear a vibrating sound as they approach so you will know when they attack. Be careful tho, because when they regain consciousness they will flail around and deal some damage, so you'll have to be careful with timing

Steel Worms: Similar attack, but does not get stunned. Much longer body tho, so you have time to attack it while it turns around for another attack. But once again, be careful, as they have spike-holes which will shoot at you.

(Rare Enemy) Cobalt Worm: Shimmering blue color, same as the Steel Worm but much tougher, and a bit faster. It's tail's orb is green. (Every other enemy's tail-orb is yellow)

(Boss) Steel Worm Queen: It's attack is similar, however, it does not get stunned if you dodge it. Because their body is long, you'll still be able to get a couple of hits in tho. It also has another attack which makes giant rocks fall from the ceiling by making the whole cave rumble.


### Drops

Steel Worm Babies: a bit of rocks and maybe some low-tier ores/metals

Steel Worms: same as previous but rare drop: "Steel Worms Orb" can be held in hand. If low-tier ore is nearby, it vibrates depending on how close. Can be crafted into "Necklace of Vibration", which has the same effect but you don't have to hold it. (both glow)

Cobalt Worms: a bit of rare materials, and very rare drop: "Cobalt Worms Orb", works the same as the previous but can detect more rare ore. Can also be crafted into a necklace ("Necklace of Greater Vibration")

Steel Worm Queens: maybe slightly rare ore, but very rare drop: "Queen Worm's Wedding Crystal"



## [Medium-Hard] Variation 2 (Finished Buffet, "Troll Age")

### Enemies

Stone Troll: big monster, large detection range unless you are crouching, then it's just normal detection range. When killed, leaves a pool of acid for a while which you should be really wary of, as standing in it will kill you swiftly.

Dwarves: short but strong humanoids, attacks with pickaxe/axe.

Boss? Dunno right now.


### Drops

ST: stones, and rare drop: "Stone Troll's Hooves" can be used to make special "shoes" for your mounts to make them faster. Only works on ground-type mounts.

Dwarves: maybe some mining equipment, and an already existing item, Dwarven Cheese.



## [Easy-Medium] Variation 3 (Nature's Overtake)

### Enemies

Just regular cave enemies

### Drops

I guess regular cave drops



## [non] Variation 4 (Filled, "Back to Whole")

Just looks like a hole that was filled in with rocks and dirt.



## [Super Hard] Variation 5 (Dwarves Halls)

### Enemies

Dwarf Soldiers: Dwarves that are prepared to fight, unlike the Dwarf Miners from Variation 2

## [Ultra Hard] Variation 6 (Queen's Grave)

### Special

"Immoral" debuff is active while in the dungeon, can't be cured, lasts for a while even after leaving the dungeon. Reduces your damage, may also cause "Trauma" which can only be removed by a specialized doctor/medic/healer/mage/wizard/etc.

### Enemies

Weeping Steel Worm Baby: Similar to the Steel Worm Babies, but the stunned time is much shorter. Slightly blue in appearance. While fighting, "Pity" debuff is activated, damage is reduced.

Mourning Steel Worm: Similar to the Steel Worms, but slightly red, much stronger, and a bit faster. When the worm hits you it also grants "Violently Bleeding" debuff.

(Rare Enemy) Cobalt Worm of Sorrow: same as the other, but an even deeper blue color. It will also make the ground vibrate, lowering your speed by 20%.

(Boss) Steel Worm King: At first, the Steel Worm King will be gently tied around the petrified body of the Steel Worm Queen, waiting for their last moments. To activate the fight, the King has to be lowered to 90% health, after which it will enter it's first phase, "Enraged". It will attack similar to how the Queen did, but it's attacks will constantly make rocks fall from the ceiling, rather than it being a separate attack. It also shoots out metal rods, which if hit you, will stay in you for a long while. Each metal rod in your body will act as -10% to your armor, capping at 50%. It will also cause you to bleed, which can't be cured.

### Drops

WIP

## Lore and information

So let me explain the variations, and why they exist. So Variation 1 is the first stage, where the Queen already dug out the main hole, and the Birthing Hole. The Queen then does the birthing and then there will Steel Worms and Steel Worm Babies, and of course rarely a Cobalt Worm. Usually Queen's only dig a couple of Buffet's before coming to an eternal rest. At that point, their Wedding Crystal will break and disappear, while the Queen turns into stone and ores.

Speaking of the Wedding Crystal, the way these creatures life cycle works is pretty interesting from what I observed. First of all, when they are born they have no gender. After a couple decades they reach adulthood, in which they will already be divided into males and females, depending on what kind of ores and crystals they consumed. After another couple decades, males will try to find large rare crystals to give to the females, after which a Queen is born. Most males are unsuccessful in finding said large rare crystals, so that's why the world wasn't devoured by insane amounts of Steel Worm Queens. After a quick "honeymoon", the Queen goes onto doing what I wrote before.

Now, once they are done digging around and stuff, which could take anywhere from a week to even a decade, they will move onto another hole, however, Baby Steel Worm's will devour any flowers nearby before leaving with the Queen. **Why is that important?** You can tell what tier the Buffet will be just by the hole's appearance. If there are flowers and rocks, then it is still active and is filled with Steel Worms.
If there are no flowers but there are rocks, then Stone Trolls took over the hole to find any left overs, and brave Dwarves also scout the hole.
If there are no flowers and no rocks, that means the Stone Trolls and Dwarves have also left already, so it just became a regular hole.


# Random Note

I honestly believe that variation is one of the most overlooked aspects of a game.
It adds so much immersion, replayability, and pretty much everything, that when done right,
it'll turn anything into remarkable. Combine that with a good game (Veloren) and you have
a recipe for legendary.


